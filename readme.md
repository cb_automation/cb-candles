```
$ cargo run -- --help
cb-candles 0.1.0
Fetches historic transaction data from coinbase APIs

USAGE:
    cb-candles [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d, --duration <duration>          How far into the past to fetch historical value data. Determines approximately
                                       how many pages to fetch [default: 1y]
    -g, --granularity <granularity>    Controls the granularity of historic data candles. Finer granularity produces
                                       more samples (rows) [default: daily]  [possible values: daily, hourly]
    -o, --output <output>              What format to write output rows [default: debug]  [possible values: debug, sql]
    -p, --product <product>            Which product (currency pair) to use [default: ETH-USD]
```

```
$ cargo run -- --output sql | sqlite3 candles.db
$ sqlite3 candles.db 'SELECT * FROM candles LIMIT 10;'
1578528000|2020-01-09 00:00:00 UTC|2020-01-10 00:00:00 UTC|141.38
1578614400|2020-01-10 00:00:00 UTC|2020-01-11 00:00:00 UTC|145.81
1578700800|2020-01-11 00:00:00 UTC|2020-01-12 00:00:00 UTC|148.0
1578787200|2020-01-12 00:00:00 UTC|2020-01-13 00:00:00 UTC|146.6
1578873600|2020-01-13 00:00:00 UTC|2020-01-14 00:00:00 UTC|147.0
1578960000|2020-01-14 00:00:00 UTC|2020-01-15 00:00:00 UTC|170.83
1579046400|2020-01-15 00:00:00 UTC|2020-01-16 00:00:00 UTC|172.18
1579132800|2020-01-16 00:00:00 UTC|2020-01-17 00:00:00 UTC|167.28
1579219200|2020-01-17 00:00:00 UTC|2020-01-18 00:00:00 UTC|174.52
1579305600|2020-01-18 00:00:00 UTC|2020-01-19 00:00:00 UTC|179.32
```

```
$ ./refresh_list.sh
+ cargo run
   Compiling cb-candles v0.1.0 (/home/blake/workspace/cb-candles)
    Finished dev [unoptimized + debuginfo] target(s) in 3.30s
     Running `target/debug/cb-candles`
$ head candles.list
((2021-08-29T00:00:00Z, 2021-08-30T00:00:00Z), 3287.73)
((2021-08-28T00:00:00Z, 2021-08-29T00:00:00Z), 3290.51)
((2021-08-27T00:00:00Z, 2021-08-28T00:00:00Z), 3283.26)
((2021-08-26T00:00:00Z, 2021-08-27T00:00:00Z), 3251.05)
((2021-08-25T00:00:00Z, 2021-08-26T00:00:00Z), 3249.0)
((2021-08-24T00:00:00Z, 2021-08-25T00:00:00Z), 3360.0)
((2021-08-23T00:00:00Z, 2021-08-24T00:00:00Z), 3378.6)
((2021-08-22T00:00:00Z, 2021-08-23T00:00:00Z), 3275.0)
((2021-08-21T00:00:00Z, 2021-08-22T00:00:00Z), 3311.0)
((2021-08-20T00:00:00Z, 2021-08-21T00:00:00Z), 3300.0)
```

```
$ cargo run -- --product "BTC-USD" | head
((2021-08-29T00:00:00Z, 2021-08-30T00:00:00Z), 49667.1)
((2021-08-28T00:00:00Z, 2021-08-29T00:00:00Z), 49309.83)
((2021-08-27T00:00:00Z, 2021-08-28T00:00:00Z), 49185.12)
((2021-08-26T00:00:00Z, 2021-08-27T00:00:00Z), 49365.1)
((2021-08-25T00:00:00Z, 2021-08-26T00:00:00Z), 49277.0)
((2021-08-24T00:00:00Z, 2021-08-25T00:00:00Z), 49875.9)
((2021-08-23T00:00:00Z, 2021-08-24T00:00:00Z), 50505.0)
((2021-08-22T00:00:00Z, 2021-08-23T00:00:00Z), 49526.13)
((2021-08-21T00:00:00Z, 2021-08-22T00:00:00Z), 49821.92)
((2021-08-20T00:00:00Z, 2021-08-21T00:00:00Z), 49400.0)
```

```
$ cargo run -- --duration 1w --granularity hourly
((2021-08-29T23:00:00Z, 2021-08-30T00:00:00Z), 3248.89)
((2021-08-29T22:00:00Z, 2021-08-29T23:00:00Z), 3259.06)
((2021-08-29T21:00:00Z, 2021-08-29T22:00:00Z), 3250.0)
((2021-08-29T20:00:00Z, 2021-08-29T21:00:00Z), 3245.58)
((2021-08-29T19:00:00Z, 2021-08-29T20:00:00Z), 3227.01)
((2021-08-29T18:00:00Z, 2021-08-29T19:00:00Z), 3211.64)
((2021-08-29T17:00:00Z, 2021-08-29T18:00:00Z), 3211.06)
((2021-08-29T16:00:00Z, 2021-08-29T17:00:00Z), 3217.27)
((2021-08-29T15:00:00Z, 2021-08-29T16:00:00Z), 3191.66)
((2021-08-29T14:00:00Z, 2021-08-29T15:00:00Z), 3201.19)
# Note: 290 more rows! Always produces multiples of 300 rows.
```
