use std::str::FromStr;
use std::time;

use chrono::{DateTime, Duration, NaiveDateTime, Utc};
use coinbase_pro_rs::{
    structs::public::{Candle, Granularity},
    ASync, Public, MAIN_URL,
};
use futures::future::join_all;
use humantime::parse_duration;
use lazy_static::lazy_static;
use structopt::StructOpt;

const PAGE_LIMIT: i64 = 300;
lazy_static! {
    static ref END_TIME: DateTime<Utc> = Utc::now();
}

#[derive(Clone, Debug)]
enum OptGranularity {
    Hourly,
    Daily,
}

#[derive(Clone, Debug)]
enum OutputStyle {
    Debug,
    Sql,
}

impl FromStr for OutputStyle {
    type Err = &'static str;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        match value {
            "debug" => Ok(OutputStyle::Debug),
            "sql" => Ok(OutputStyle::Sql),
            _ => Err("Unrecognised output style"),
        }
    }
}

impl From<OptGranularity> for Granularity {
    fn from(val: OptGranularity) -> Granularity {
        match val {
            OptGranularity::Hourly => Granularity::H1,
            OptGranularity::Daily => Granularity::D1,
        }
    }
}

impl FromStr for OptGranularity {
    type Err = &'static str;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        match value {
            "daily" => Ok(OptGranularity::Daily),
            "hourly" => Ok(OptGranularity::Hourly),
            _ => Err("Unrecognised granularity"),
        }
    }
}

/// Fetches historic transaction data from coinbase APIs.
#[derive(StructOpt, Debug)]
#[structopt(name = "cb-candles")]
struct Opt {
    /// Which product (currency pair) to use.
    #[structopt(short, long, default_value = "ETH-USD")]
    product: String,

    /// How far into the past to fetch historical value data. Determines approximately how many pages to fetch.
    #[structopt(short, long, default_value = "1y", parse(try_from_str = parse_duration))]
    duration: time::Duration,

    /// Controls the granularity of historic data candles. Finer granularity produces more samples (rows).
    #[structopt(short, long, default_value = "daily", possible_values(&["daily", "hourly"]), parse(try_from_str))]
    granularity: OptGranularity,

    /// What format to write output rows.
    #[structopt(short, long, default_value = "debug", possible_values(&["debug", "sql"]), parse(try_from_str))]
    output: OutputStyle,
}

trait CandleDetailable {
    fn window_start(&self) -> DateTime<Utc>;
    fn window_covered(&self, granularity: Granularity) -> (DateTime<Utc>, DateTime<Utc>);
    fn val_window_min(&self) -> f64;
    fn val_window_max(&self) -> f64;
    fn val_window_first(&self) -> f64;
    fn val_window_last(&self) -> f64;
    fn val_window_volume_sum(&self) -> f64;
}

impl CandleDetailable for Candle {
    fn window_start(&self) -> DateTime<Utc> {
        DateTime::from_utc(NaiveDateTime::from_timestamp(self.0 as i64, 0), Utc)
    }

    fn window_covered(&self, granularity: Granularity) -> (DateTime<Utc>, DateTime<Utc>) {
        match granularity {
            Granularity::D1 => (self.window_start(), self.window_start() + Duration::days(1)),
            Granularity::H1 => (
                self.window_start(),
                self.window_start() + Duration::hours(1),
            ),
            _ => unimplemented!(),
        }
    }

    fn val_window_min(&self) -> f64 {
        self.1
    }

    fn val_window_max(&self) -> f64 {
        self.2
    }

    fn val_window_first(&self) -> f64 {
        self.3
    }

    fn val_window_last(&self) -> f64 {
        self.4
    }

    fn val_window_volume_sum(&self) -> f64 {
        self.5
    }
}

/// 1y   300d -> 2 pages
/// x  =
///
/// 1y   300h -> 30 pages
/// x  =
fn determine_windows(
    opt_granularity: &OptGranularity,
    period: &time::Duration,
) -> Vec<(DateTime<Utc>, DateTime<Utc>)> {
    let page_fn = match opt_granularity {
        OptGranularity::Daily => Duration::days,
        OptGranularity::Hourly => Duration::hours,
    };

    let max_pages =
        (period.as_secs() as f64 / page_fn(PAGE_LIMIT).num_seconds() as f64).ceil() as i64;

    (0..max_pages)
        .rev()
        .map(|page| {
            (
                *END_TIME - page_fn((page + 1) * PAGE_LIMIT),
                *END_TIME - page_fn(page * PAGE_LIMIT),
            )
        })
        .collect()
}

type CandlesTuple = ((DateTime<Utc>, DateTime<Utc>), f64);

fn emit(results: &[CandlesTuple], output: OutputStyle) {
    match output {
        OutputStyle::Debug => {
            results
                .iter()
                .for_each(|candle_detail| println!("{:?}", candle_detail));
        }
        OutputStyle::Sql => {
            println!("BEGIN TRANSACTION;");
            println!("CREATE TABLE candles (timestamp INTEGER PRIMARY KEY, start TEXT, end TEXT, max REAL);");
            results.iter().for_each(|((start, end), val)| {
                println!(
                    "INSERT INTO candles (timestamp, start, end, max) VALUES ({}, {}, {}, {});",
                    start.timestamp(),
                    format!("\"{}\"", start),
                    format!("\"{}\"", end),
                    val
                )
            });
            println!("COMMIT;");
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let candle_details = determine_windows(&opt.granularity, &opt.duration)
        .into_iter()
        .map(|(start, end)| {
            let product = opt.product.clone();
            let granularity = opt.granularity.clone();
            tokio::spawn(async move {
                let client: Public<ASync> = Public::new_with_keep_alive(MAIN_URL, false);
                client
                    .get_candles(&product, Some(start), Some(end), granularity.clone().into())
                    .await
                    .expect("failed to await candles")
                    .into_iter()
                    .map(move |candle| {
                        (
                            candle.window_covered(granularity.clone().into()),
                            candle.val_window_max(),
                        )
                    })
            })
        })
        .collect::<Vec<_>>();

    let results = join_all(candle_details)
        .await
        .into_iter()
        .map(|vals| vals.expect("Failed to join"))
        .flatten()
        .collect::<Vec<_>>();

    emit(&results, opt.output);

    Ok(())
}
